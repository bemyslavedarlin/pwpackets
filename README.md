# README #

### What is this repository for? ###

* Wrapper for pwpackets classes
* Version 1.0.0

### How do I get set up? ###

* To add new methods, go to the **/methods/operations/*.php**
* Configuration located at **/libs/config.php**
* Database configuration located at **/libs/config.php**
* To run tests, open** index.php**