<?
/**
 * Базовая конфигурация
*/
$arConfig = array(
	'host' => "localhost",
	'port' => 29400,
	'dbName' => "dbpw",
	'dbUser' => "root",
	'dbPasswd' => "",
);


$dsn = "mysql:host=".$arConfig['host'].";dbname=".$arConfig['dbName'].";charset=utf-8";
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);
$PDO = new PDO($dsn, $arConfig['dbUser'], $arConfig['dbPasswd'], $opt);
?>