<?
/*
Автор: Гадя
Функции для дебага
*/
if(!function_exists('_vardump')){
	/*
	 * Custom vardump
	 *
	 * @param mixed $var
	 * @param string $output ('f' or 'd')
	 */
	function _vardump($var = '', $output = 'display'){

		$dump = print_r($var, true);

		$backtraceInfo = debug_backtrace(false);
		$source = 'File <b>'.$backtraceInfo[0]['file'].'</b> in line <b>'.$backtraceInfo[0]['line'].'</b>';

		$dump = '<div style="margin: 20px; background: #fdf5db; border-radius: 4px; padding: 5px; border: 1px solid #ffe69d; box-shadow: 0 0 10px 0px #ccc; font-family: Arial;"><div style="margin-bottom: 5px; font-size: 11px; color: #848484; overflow:hidden;" title="'.strip_tags($source).'">'.$source.'</div><pre style="display: block; margin: 0; padding: 10px;  background: #fff; border: 1px solid #ccc; max-height: 400px; overflow: scroll; font-size: 13px; color: #000;">'.$dump.'</pre></div>';
		echo $dump;
	}
}

?>