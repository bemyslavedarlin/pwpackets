<?
/*
Автор: Desmond Hume
Skype: desmondhume777
Функции управления данными персонажа через php (с использованием PacketClassPW)
Спасибки принимаю на webmoney: R886298849911 или Z152378827400
*/

function GetUserRoles($userid)
{
   $obWriter = new WritePacket();
   $obWriter->WriteUInt32(-1);
   $obWriter->WriteUInt32($userid);
   $obWriter->Pack(0xD49);
 
	if (!$obWriter->Send($arConfig['host'], $arConfig['port']))
     return -1;
 
   $obReader = new ReadPacket($obWriter);
   $obReader->ReadPacketInfo();
   $obReader->ReadUInt32();
   $obReader->ReadUInt32();
   $rolescount = $obReader->ReadCUInt32();
   for ($i = 0; $i < $rolescount; $i++)
   {
     $role['ID'] = $obReader->ReadUInt32();
     $role['Name'] = $obReader->ReadUString();
     $roles[] = $role;
   }
   return $roles;
}

?>