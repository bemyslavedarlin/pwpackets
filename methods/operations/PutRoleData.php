<?
/*
Автор: Desmond Hume
Skype: desmondhume777
Функции управления данными персонажа через php (с использованием PacketClassPW)
Спасибки принимаю на webmoney: R886298849911 или Z152378827400
*/

function PutRoleData($roleid, $arRoleData)
{
	//init writer
	$obWriter = new WritePacket();
	$obWriter->WriteUInt32(-1);
	$obWriter->WriteUInt32($roleid);
	$obWriter->WriteUByte(1); // overwrite

	$obWriter->WriteUByte($arRoleData['base']['version']);
	$obWriter->WriteUInt32($arRoleData['base']['id']);
	$obWriter->WriteUString($arRoleData['base']['name']);
	$obWriter->WriteUInt32($arRoleData['base']['race']);
	$obWriter->WriteUInt32($arRoleData['base']['cls']);
	$obWriter->WriteUByte($arRoleData['base']['gender']);
	$obWriter->WriteOctets($arRoleData['base']['custom_data']);
	$obWriter->WriteOctets($arRoleData['base']['config_data']);
	$obWriter->WriteUInt32($arRoleData['base']['custom_stamp']);
	$obWriter->WriteUByte($arRoleData['base']['status']);
	$obWriter->WriteUInt32($arRoleData['base']['delete_time']);
	$obWriter->WriteUInt32($arRoleData['base']['create_time']);
	$obWriter->WriteUInt32($arRoleData['base']['lastlogin_time']);

	$obWriter->WriteCUInt32(count($arRoleData['base']['forbid']));

	foreach ($arRoleData['base']['forbid'] as $value)
	{
		$obWriter->WriteUByte($value['type']);
		$obWriter->WriteUInt32($value['time']);
		$obWriter->WriteUInt32($value['createtime']);
		$obWriter->WriteUString($value['reason']);
	}
	
	$obWriter->WriteOctets($arRoleData['base']['help_states']);
	$obWriter->WriteUInt32($arRoleData['base']['spouse']);
	$obWriter->WriteUInt32($arRoleData['base']['userid']);
	$obWriter->WriteOctets($arRoleData['base']['cross_data']);
	$obWriter->WriteUByte($arRoleData['base']['reserved2']);
	$obWriter->WriteUByte($arRoleData['base']['reserved3']);
	$obWriter->WriteUByte($arRoleData['base']['reserved4']);
	$obWriter->WriteUByte($arRoleData['status']['version']);
	$obWriter->WriteUInt32($arRoleData['status']['level']);
	$obWriter->WriteUInt32($arRoleData['status']['level2']);
	$obWriter->WriteUInt32($arRoleData['status']['exp']);
	$obWriter->WriteUInt32($arRoleData['status']['sp']);
	$obWriter->WriteUInt32($arRoleData['status']['pp']);
	$obWriter->WriteUInt32($arRoleData['status']['hp']);
	$obWriter->WriteUInt32($arRoleData['status']['mp']);
	$obWriter->WriteFloat($arRoleData['status']['posx']);
	$obWriter->WriteFloat($arRoleData['status']['posy']);
	$obWriter->WriteFloat($arRoleData['status']['posz']);
	$obWriter->WriteUInt32($arRoleData['status']['worldtag']);
	$obWriter->WriteUInt32($arRoleData['status']['invader_state']);
	$obWriter->WriteUInt32($arRoleData['status']['invader_time']);
	$obWriter->WriteUInt32($arRoleData['status']['pariah_time']);
	$obWriter->WriteUInt32($arRoleData['status']['reputation']);
	$obWriter->WriteOctets($arRoleData['status']['custom_status']);
	$obWriter->WriteOctets($arRoleData['status']['filter_data']);
	$obWriter->WriteOctets($arRoleData['status']['charactermode']);
	$obWriter->WriteOctets($arRoleData['status']['instancekeylist']);
	$obWriter->WriteUInt32($arRoleData['status']['dbltime_expire']);
	$obWriter->WriteUInt32($arRoleData['status']['dbltime_mode']);
	$obWriter->WriteUInt32($arRoleData['status']['dbltime_begin']);
	$obWriter->WriteUInt32($arRoleData['status']['dbltime_used']);
	$obWriter->WriteUInt32($arRoleData['status']['dbltime_max']);
	$obWriter->WriteUInt32($arRoleData['status']['time_used']);
	$obWriter->WriteOctets($arRoleData['status']['dbltime_data']);
	$obWriter->WriteUInt16($arRoleData['status']['storesize']);
	$obWriter->WriteOctets($arRoleData['status']['petcorral']);
	$obWriter->WriteOctets($arRoleData['status']['property']);
	$obWriter->WriteOctets($arRoleData['status']['var_data']);
	$obWriter->WriteOctets($arRoleData['status']['skills']);
	$obWriter->WriteOctets($arRoleData['status']['storehousepasswd']);
	$obWriter->WriteOctets($arRoleData['status']['waypointlist']);
	$obWriter->WriteOctets($arRoleData['status']['coolingtime']);
	$obWriter->WriteOctets($arRoleData['status']['npc_relation']);
	$obWriter->WriteOctets($arRoleData['status']['multi_exp_ctrl']);
	$obWriter->WriteOctets($arRoleData['status']['storage_task']);
	$obWriter->WriteOctets($arRoleData['status']['faction_contrib']);
	$obWriter->WriteOctets($arRoleData['status']['force_data']);
	$obWriter->WriteOctets($arRoleData['status']['online_award']);
	$obWriter->WriteOctets($arRoleData['status']['profit_time_data']);
	$obWriter->WriteOctets($arRoleData['status']['country_data']);
	$obWriter->WriteOctets($arRoleData['status']['king_data']);
	$obWriter->WriteOctets($arRoleData['status']['meridian_data']);
	$obWriter->WriteOctets($arRoleData['status']['extraprop']);
	$obWriter->WriteOctets($arRoleData['status']['title_data']);
	$obWriter->WriteOctets($arRoleData['status']['reincarnation_data']);
	$obWriter->WriteOctets($arRoleData['status']['realm_data']);
	$obWriter->WriteUByte($arRoleData['status']['reserved2']);
	$obWriter->WriteUByte($arRoleData['status']['reserved3']);
	$obWriter->WriteUInt32($arRoleData['pocket']['capacity']);
	$obWriter->WriteUInt32($arRoleData['pocket']['timestamp']);
	$obWriter->WriteUInt32($arRoleData['pocket']['money']);

	$obWriter->WriteCUInt32(count($arRoleData['pocket']['items']));

	foreach ($arRoleData['pocket']['items'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->WriteUInt32($arRoleData['pocket']['reserved1']);
	$obWriter->WriteUInt32($arRoleData['pocket']['reserved2']);

	$obWriter->WriteCUInt32(count($arRoleData['equipment']['inv']));

	foreach ($arRoleData['equipment']['inv'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}
	
	$obWriter->WriteUInt32($arRoleData['storehouse']['capacity']);
	$obWriter->WriteUInt32($arRoleData['storehouse']['money']);

	$obWriter->WriteCUInt32(count($arRoleData['storehouse']['items']));

	foreach ($arRoleData['storehouse']['items'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->WriteUByte($arRoleData['storehouse']['size1']);
	$obWriter->WriteUByte($arRoleData['storehouse']['size2']);

	$obWriter->WriteCUInt32(count($arRoleData['storehouse']['dress']));

	foreach ($arRoleData['storehouse']['dress'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->WriteCUInt32(count($arRoleData['storehouse']['material']));

	foreach ($arRoleData['storehouse']['material'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->WriteUByte($arRoleData['storehouse']['size3']);

	$obWriter->WriteCUInt32(count($arRoleData['storehouse']['generalcard']));

	foreach ($arRoleData['storehouse']['generalcard'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->WriteUInt16($arRoleData['storehouse']['reserved']);
	$obWriter->WriteOctets($arRoleData['task']['task_data']);
	$obWriter->WriteOctets($arRoleData['task']['task_complete']);
	$obWriter->WriteOctets($arRoleData['task']['task_finishtime']);

	$obWriter->WriteCUInt32(count($arRoleData['task']['task_inventory']));

	foreach ($arRoleData['task']['task_inventory'] as $value)
	{
		$obWriter->WriteUInt32($value['id']);
		$obWriter->WriteUInt32($value['pos']);
		$obWriter->WriteUInt32($value['count']);
		$obWriter->WriteUInt32($value['maxcount']);
		$obWriter->WriteOctets($value['data']);
		$obWriter->WriteUInt32($value['proctype']);
		$obWriter->WriteUInt32($value['expire_date']);
		$obWriter->WriteUInt32($value['guid1']);
		$obWriter->WriteUInt32($value['guid2']);
		$obWriter->WriteUInt32($value['mask']);
	}

	$obWriter->Pack(0x1F42);
	return $obWriter->Send($arConfig['host'], $arConfig['port']);
}
?>