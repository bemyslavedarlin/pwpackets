<?
/*
Автор: Desmond Hume
Skype: desmondhume777
Функции управления данными персонажа через php (с использованием PacketClassPW)
Спасибки принимаю на webmoney: R886298849911 или Z152378827400
*/

function GetRoleData($roleid)
{
	//init writer
	$obWriter = new WritePacket();
	$obWriter->WriteUInt32(-1);
	$obWriter->WriteUInt32($roleid);
	$obWriter->Pack(0x1F43);
	if (!$obWriter->Send($arConfig['host'], $arConfig['port']))
		return -1;
	
	//init reader
	$obReader = new ReadPacket($obWriter);
	$obReader->ReadPacketInfo();
	$obReader->ReadUInt32(); // -1
	$obReader->ReadUInt32(); // retcode
	$arRoleData['base']['version'] = $obReader->ReadUByte();
	$arRoleData['base']['id'] = $obReader->ReadUInt32();
	$arRoleData['base']['name'] = $obReader->ReadUString();
	$arRoleData['base']['race'] = $obReader->ReadUInt32();
	$arRoleData['base']['cls'] = $obReader->ReadUInt32();
	$arRoleData['base']['gender'] = $obReader->ReadUByte();
	$arRoleData['base']['custom_data'] = $obReader->ReadOctets();
	$arRoleData['base']['config_data'] = $obReader->ReadOctets();
	$arRoleData['base']['custom_stamp'] = $obReader->ReadUInt32();
	$arRoleData['base']['status'] = $obReader->ReadUByte();
	$arRoleData['base']['delete_time'] = $obReader->ReadUInt32();
	$arRoleData['base']['create_time'] = $obReader->ReadUInt32();
	$arRoleData['base']['lastlogin_time'] = $obReader->ReadUInt32();
	$forbidCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $forbidCount; $i++)
	{
		$arRoleForbid['type'] = $obReader->ReadUByte();
		$arRoleForbid['time'] = $obReader->ReadUInt32();
		$arRoleForbid['createtime'] = $obReader->ReadUInt32();
		$arRoleForbid['reason'] = $obReader->ReadUString();
		$arRoleData['base']['forbid'][] = $arRoleForbid;
	}

	$arRoleData['base']['help_states'] = $obReader->ReadOctets();
	$arRoleData['base']['spouse'] = $obReader->ReadUInt32();
	$arRoleData['base']['userid'] = $obReader->ReadUInt32();
	$arRoleData['base']['cross_data'] = $obReader->ReadOctets();
	$arRoleData['base']['reserved2'] = $obReader->ReadUByte();
	$arRoleData['base']['reserved3'] = $obReader->ReadUByte();
	$arRoleData['base']['reserved4'] = $obReader->ReadUByte();
	$arRoleData['status']['version'] = $obReader->ReadUByte();
	$arRoleData['status']['level'] = $obReader->ReadUInt32();
	$arRoleData['status']['level2'] = $obReader->ReadUInt32();
	$arRoleData['status']['exp'] = $obReader->ReadUInt32();
	$arRoleData['status']['sp'] = $obReader->ReadUInt32();
	$arRoleData['status']['pp'] = $obReader->ReadUInt32();
	$arRoleData['status']['hp'] = $obReader->ReadUInt32();
	$arRoleData['status']['mp'] = $obReader->ReadUInt32();
	$arRoleData['status']['posx'] = $obReader->ReadFloat();
	$arRoleData['status']['posy'] = $obReader->ReadFloat();
	$arRoleData['status']['posz'] = $obReader->ReadFloat();
	$arRoleData['status']['worldtag'] = $obReader->ReadUInt32();
	$arRoleData['status']['invader_state'] = $obReader->ReadUInt32();
	$arRoleData['status']['invader_time'] = $obReader->ReadUInt32();
	$arRoleData['status']['pariah_time'] = $obReader->ReadUInt32();
	$arRoleData['status']['reputation'] = $obReader->ReadUInt32();
	$arRoleData['status']['custom_status'] = $obReader->ReadOctets();
	$arRoleData['status']['filter_data'] = $obReader->ReadOctets();
	$arRoleData['status']['charactermode'] = $obReader->ReadOctets();
	$arRoleData['status']['instancekeylist'] = $obReader->ReadOctets();
	$arRoleData['status']['dbltime_expire'] = $obReader->ReadUInt32();
	$arRoleData['status']['dbltime_mode'] = $obReader->ReadUInt32();
	$arRoleData['status']['dbltime_begin'] = $obReader->ReadUInt32();
	$arRoleData['status']['dbltime_used'] = $obReader->ReadUInt32();
	$arRoleData['status']['dbltime_max'] = $obReader->ReadUInt32();
	$arRoleData['status']['time_used'] = $obReader->ReadUInt32();
	$arRoleData['status']['dbltime_data'] = $obReader->ReadOctets();
	$arRoleData['status']['storesize'] = $obReader->ReadUInt16();
	$arRoleData['status']['petcorral'] = $obReader->ReadOctets();
	$arRoleData['status']['property'] = $obReader->ReadOctets();
	$arRoleData['status']['var_data'] = $obReader->ReadOctets();
	$arRoleData['status']['skills'] = $obReader->ReadOctets();
	$arRoleData['status']['storehousepasswd'] = $obReader->ReadOctets();
	$arRoleData['status']['waypointlist'] = $obReader->ReadOctets();
	$arRoleData['status']['coolingtime'] = $obReader->ReadOctets();
	$arRoleData['status']['npc_relation'] = $obReader->ReadOctets();
	$arRoleData['status']['multi_exp_ctrl'] = $obReader->ReadOctets();
	$arRoleData['status']['storage_task'] = $obReader->ReadOctets();
	$arRoleData['status']['faction_contrib'] = $obReader->ReadOctets();
	$arRoleData['status']['force_data'] = $obReader->ReadOctets();
	$arRoleData['status']['online_award'] = $obReader->ReadOctets();
	$arRoleData['status']['profit_time_data'] = $obReader->ReadOctets();
	$arRoleData['status']['country_data'] = $obReader->ReadOctets();
	$arRoleData['status']['king_data'] = $obReader->ReadOctets();
	$arRoleData['status']['meridian_data'] = $obReader->ReadOctets();
	$arRoleData['status']['extraprop'] = $obReader->ReadOctets();
	$arRoleData['status']['title_data'] = $obReader->ReadOctets();
	$arRoleData['status']['reincarnation_data'] = $obReader->ReadOctets();
	$arRoleData['status']['realm_data'] = $obReader->ReadOctets();
	$arRoleData['status']['reserved2'] = $obReader->ReadUByte();
	$arRoleData['status']['reserved3'] = $obReader->ReadUByte();
	$arRoleData['pocket']['capacity'] = $obReader->ReadUInt32();
	$arRoleData['pocket']['timestamp'] = $obReader->ReadUInt32();
	$arRoleData['pocket']['money'] = $obReader->ReadUInt32();
	$inventoryPocketCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $inventoryPocketCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['pocket']['items'][] = $arRoleItems;
	}

	$arRoleData['pocket']['reserved1'] = $obReader->ReadUInt32();
	$arRoleData['pocket']['reserved2'] = $obReader->ReadUInt32();
	$inventoryEquipCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $inventoryEquipCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['equipment']['inv'][] = $arRoleItems;
	}

	$arRoleData['storehouse']['capacity'] = $obReader->ReadUInt32();
	$arRoleData['storehouse']['money'] = $obReader->ReadUInt32();
	$storeHouseCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $storeHouseCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['storehouse']['items'][] = $arRoleItems;
	}

	$arRoleData['storehouse']['size1'] = $obReader->ReadUByte();
	$arRoleData['storehouse']['size2'] = $obReader->ReadUByte();
	$dressCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $dressCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['storehouse']['dress'][] = $arRoleItems;
	}

	$materialsCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $materialsCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['storehouse']['material'][] = $arRoleItems;
	}

	$arRoleData['storehouse']['size3'] = $obReader->ReadUByte();
	$generalCardCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $generalCardCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['storehouse']['generalcard'][] = $arRoleItems;
	}

	$arRoleData['storehouse']['reserved'] = $obReader->ReadUInt16();
	$arRoleData['task']['task_data'] = $obReader->ReadOctets();
	$arRoleData['task']['task_complete'] = $obReader->ReadOctets();
	$arRoleData['task']['task_finishtime'] = $obReader->ReadOctets();
	$taskInventoryCount = $obReader->ReadCUInt32();

	for ($i = 0; $i < $taskInventoryCount; $i++)
	{
		$arRoleItems['id'] = $obReader->ReadUInt32();
		$arRoleItems['pos'] = $obReader->ReadUInt32();
		$arRoleItems['count'] = $obReader->ReadUInt32();
		$arRoleItems['maxcount'] = $obReader->ReadUInt32();
		$arRoleItems['data'] = $obReader->ReadOctets();
		$arRoleItems['proctype'] = $obReader->ReadUInt32();
		$arRoleItems['expire_date'] = $obReader->ReadUInt32();
		$arRoleItems['guid1'] = $obReader->ReadUInt32();
		$arRoleItems['guid2'] = $obReader->ReadUInt32();
		$arRoleItems['mask'] = $obReader->ReadUInt32();
		$arRoleData['task']['task_inventory'][] = $arRoleItems;
	}

	return $arRoleData;
}

?>