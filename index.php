<?
//ini_set('display_errors', 2);
//error_reporting(E_ALL); 
	
//Подключаем конфиг
include 'libs/config.php';

//Подключаем читалку и писалку
include 'libs/Reader.php';
include 'libs/Writer.php';

//Подключаем кастомные методы
include 'methods/debug.php';

//Подключаем методы управления данными
foreach (glob("methods/operations/*.php") as $filename)
{
    include $filename;
}

/* Операции получения данных */
//Массив входных данных, можно задавать из post/get данных формы
$arRoleInput = array(
	'id' => 1024, //Идентификатор персонажа
	'items' => array(1111, 2222, 3333, 4444), //Массив предметов для манипуляций
	'money' = 0, //Юани
	//И так далее
);

//Иницилизируем метод получения данных
$arRoleData = GetRoleData($arRoleInput['id']);

/* Читаем данные */
_vardump($arRoleData['base']); //Базовая информация персонажа
_vardump($arRoleData['pocket']); //Сумка персонажа
_vardump($arRoleData['equipment']); //Снаряжение персонажа (что надето)
//и тд.

/* Пишем данные */
//Обнуление юаней
$arRoleData['storehouse']['money'] = $arRoleInput['money'];
PutRoleData($arRoleInput['id'], $arRoleData);

//Обнуление репутации
$arRoleData['status']['reputation']  = $arRoleInput['money'];
PutRoleData($arRoleInput['id'], $arRoleData);

//Массовое удаление предметов
$arItemsLocs = array(
  array('pocket','items'),
  array('equipment','inv'),
  array('storehouse','items'),
);
$objUsers = $PDO->query("select `ID` from `users`");
while ($user = $objUsers->fetch())
{
    $arRoles = GetUserRoles($user['ID']);
    if ($arRoles == null) continue;

    foreach ($arRoles as $arRole)
    {
        $arRoleData = GetRoleData($arRole['ID']);
        foreach($arItemsLocs as $itemLoc){
            foreach ($arRoleData[$itemLoc[0]][$itemLoc[1]] as &$value){
                if (in_array($value['id'], $arRoleInput['items']){
                    $value['id'] = 0;
                }
            }
            unset($value);
        }
        PutRoleData($arRole['ID'], $arRoleData);
    }
}
//и тд.

?>